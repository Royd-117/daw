<?php 
	$arreglo1=array(13,15,14,30,14);
	$arreglo2=array(1,2,3,4,5);
	function promedio($x){
		$prom = 0;
		for ($i=0; $i < count($x) ; $i++) { 
			$prom+=$x[$i];
		}
		return $prom/count($x);
	}
	function mediana($x){
		sort($x);
		$count=count($x);
		$medio = floor(($count-1)/2);
		if($count % 2) {
		$mediana = $x[$medio];
		} else {
		$bajo = $x[$medio];
		$alto = $x[$medio+1];
		$mediana = (($bajo+$alto)/2);
		}
		return $mediana;
	}
	function imprimetodo($x){
		sort($x);
		echo "<ul>";
		echo "Arreglo ordenado menor a mayor";
		for ($i=0; $i < count($x) ; $i++){
			$actual=$i;
			echo "<li>".$x[$actual]. "</li> ";
		}
		rsort($x);
		echo "Arreglo ordenado mayor a menor";
		for ($i=0; $i < count($x) ; $i++){ 
			$actual=$i;
			echo "<li>".$x[$actual]. "</li> ";
		}
		echo "Promedio";
		echo "<li>" .promedio($x) ."</li>";
		echo "Mediana";
		echo "<li>" .mediana($x) ."</li>";
		echo "</ul>";

	}	
	echo imprimetodo($arreglo1);
	function table($n){
		for ($i=1; $i <= $n; $i++) { 
			$cuadrado=$i*$i;
			$cubo=$i*$i*$i;
			echo "  <tr>
						<td>".$i."</td>
				    	<td>".$cuadrado."</td>
				    	<td>".$cubo."</td>
				    </tr>";
		}
	}
	echo "<table>
	<tr>
		<th>N</th>
		<th>N^2</th>
		<th>N^3</th>
	</tr>";
	echo table(10);
	echo "</table>";
?>

