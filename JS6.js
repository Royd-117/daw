var DefaultFont = document.getElementById("text1").style.fontFamily;
var OriginalText= document.getElementById("text2").innerHTML;
var MarcaDeAgua= document.getElementById("MarcaDeAgua");
var MarcaDeAguaStyle={  
  color: 'rgb(255, 165, 0)',
  zIndex: '-1',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  position: 'fixed',
  top: '0',
  right: '0',
  bottom: '0',
  left: '0'
};
Object.assign(MarcaDeAgua.style, MarcaDeAguaStyle);
document.getElementById("text2").onmouseover = function() {mouseOver2()};
document.getElementById("text2").onmouseout = function() {mouseOut2()};
document.getElementById("text1").onmouseover = function() {mouseOver()};
document.getElementById("text1").onmouseout = function() {mouseOut()};
function mouseOver() {
  document.getElementById("text1").style.fontFamily="Lucida Console, Courier, monospace";
}
function mouseOut() {
  document.getElementById("text1").style.fontFamily=DefaultFont;
}
function mouseOver2() {
  document.getElementById("text2").innerHTML="Obten más información en https://developer.mozilla.org/es/docs/Web/HTTP/Methods";
}
function mouseOut2() {
  document.getElementById("text2").innerHTML=OriginalText;
}
document.getElementById("text1").onload = move();
function move() {
  var elem = document.getElementById("myBar");   
  var width = 0;
  var id = setInterval(frame, 100);
  function frame() {
    if (width == 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}
document.getElementById("text1").onload = alertTimeout();

var time;

function alertTimeout() {
  time = setTimeout(alertFunc, 10000);
}

function alertFunc() {
  alert("Se acabo el tiempo para leer el texto");
}
